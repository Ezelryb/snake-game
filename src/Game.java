public class Game {
    private int width;
    private int height;

    public Game(int width, int height){
        this.width = width;
        this.height = height;
    }

    public String[][] createBoard(){
        String[][] board = new String[height][width];
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                board[i][j] = " ";
            }
        }
        return board;
    }

    public void render(){
        System.out.println("Width:" + this.width);
        System.out.println("Height:" + this.height);

        String[][] board = createBoard();
        System.out.println("+" + "-".repeat(width) + "+");
        for(int i=0;i<height;i++){
            System.out.print("|");
            for(int j=0;j<width;j++){
                System.out.print(board[i][j]);
            }
            System.out.print("| \n");
        }
        System.out.println("+" + "-".repeat(width) + "+");

    }
}
